import argparse
import os
from random import randrange

parser = argparse.ArgumentParser(
                    prog = 'HPC runner',
                    description = 'Test HPC runner with one input dataset and one output dataset')

# Subfolder on HPC
hpc_host = "mfiosin@gwdu101.gwdg.de"
hpc_base_path = "~/semares_workfows"
workflow_url = "https://gitlab.com/maksims.fiosins/pyrid_semares.git"

# Script folder and command
# script_folder = os.path.dirname(os.path.realpath(__file__))+"/script"
# script_command = "python3"
# script_file = "script_run.py"

# Define input and output datasets, as well as other arguments
input_folders = []
input_files = ['config', 'config_2', 'compartments_file']
output_folder = 'output_folder'
possible_empty_args = ['config_2']
#other_arguments = ["arg1", "arg2"]
other_arguments = ["compartments_scale", "simulation_name"]

# Defininig arguments
for arg in input_folders + input_files + other_arguments:
   if arg in possible_empty_args:
       parser.add_argument(f'--{arg}', nargs='?')
   else:
       parser.add_argument(f'--{arg}')

parser.add_argument(f'--{output_folder}')

args = parser.parse_args()

run_id = str(randrange(100000000,999999999))
print(f"Run id is {run_id}")

# Create folders on HPC
print("Creating folders ...")
os.system(f"ssh {hpc_host} 'mkdir -p {hpc_base_path}/{run_id}/data/'")
#os.system(f"ssh {hpc_host} 'mkdir -p {hpc_base_path}/{run_id}/bin/'")

#if getattr(args,output_dataset) is not None:
#    os.system(f"ssh {hpc_host} 'mkdir -p {hpc_base_path}/{run_id}/data/{output_dataset}'")

# Pulling workflow
#print("Pulling workflow ...")
#os.system(f'cd {hpc_base_path}; git pull ')

# Copy data
print("Copying input folders...")
for arg in input_folders:
    if getattr(args,arg) is not None:
        folder_path = getattr(args,arg)
        os.system(f'rsync -avz {folder_path}/ {hpc_host}:{hpc_base_path}/{run_id}/data/{arg}/')
    else:
        print(f"Warning: input folder {arg} not synchronized")
print("Input folders done")

print("Copying input files...")
for arg in input_files:
    if getattr(args,arg) is not None:
        file_path = getattr(args,arg)
        os.system(f'rsync -avz {file_path} {hpc_host}:{hpc_base_path}/{run_id}/data/{arg}')
    else:
        print(f"Warning: input file {arg} not synchronized")
print("Input files done")

# Executing script
print("Executing script ...")
param_str = ""
for arg in input_folders:
    if getattr(args,arg) is not None:
       param_str += f" --{arg} {hpc_base_path}/{run_id}/data/{arg}"
       
param_str = ""
for arg in input_files:
    if getattr(args,arg) is not None:
       param_str += f" --{arg} {hpc_base_path}/{run_id}/data/{arg}"

for arg in other_arguments:
    if getattr(args,arg) is not None:
       arg_value = getattr(args,arg)
       param_str += f" --{arg} {arg_value}"

if getattr(args,output_folder) is not None:
   param_str += f" --{output_folder} {hpc_base_path}/{run_id}/data/{output_folder}"

exit_code = os.system(f"ssh {hpc_host} 'source /etc/profile; module load nextflow/21.04.3; module load singularity/3.8.5; export NXF_SINGULARITY_CACHEDIR=$HOME/singulatity_cache; cd -P {hpc_base_path}/{run_id}; nextflow run {workflow_url} {param_str} -r main'")
#if exit_code != 0:
#   raise ChildProcessError(f"error in remote subprocess, exit code: {exit_code}")

# Copying output back
print("Copying output ...")
output_local_path = dataset_path = getattr(args,output_folder)
os.system(f'rsync -avz {hpc_host}:{hpc_base_path}/{run_id}/data/{output_folder}/ {output_local_path}')

# Removing hpc data
print("Removing data ...")
os.system(f"ssh {hpc_host} 'rm -rf {hpc_base_path}/{run_id}'")

print("Finished")
