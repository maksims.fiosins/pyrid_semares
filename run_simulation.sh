# For singularity, we should avoid symlink folders
cd `pwd -P`
nextflow main.nf --compartment Compartments/DendriticSpine.obj --output_folder output