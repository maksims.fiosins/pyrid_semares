nextflow.enable.dsl=2

def processed_params = [:]
for (param in params){
  processed_params[param.key] = param.value
}

file_params = ["config", "config_2", "compartments_file", "output_folder"]
for (param in file_params){
  processed_params[param] = processed_params[param] ? file(processed_params[param]) : ""
}

default_string_params = []
for (param in default_string_params){
  processed_params[param] = (processed_params.containsKey(param) && (processed_params[param] != true)) ? processed_params[param] : ""
}

script = file("simulation.py")

process simulation {
    container 'registry.gitlab.com/maksims.fiosins/pyrid_semares:1.0.2'
    publishDir "${processed_params['output_folder']}", mode: 'copy', saveAs: { fn -> fn.substring(fn.lastIndexOf('/')+1) }

    input:
       val processed_params
       file script

    output:
       path "output_folder/*"

    """
     simulation.py \\
            --config ${processed_params["config"]} \\
            --config_2 ${processed_params["config_2"]} \\
            --compartments_file ${processed_params["compartments_file"]} \\
            --compartments_scale ${processed_params["compartments_scale"]} \\
            --output_folder output_folder \\
            --simulation_name ${processed_params["simulation_name"]}
    """
}

workflow {
   simulation(processed_params, script)
}

