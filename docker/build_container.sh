docker build --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy ./ -t registry.gitlab.com/maksims.fiosins/pyrid_semares:1.0.2
docker login registry.gitlab.com
docker push registry.gitlab.com/maksims.fiosins/pyrid_semares:1.0.2
