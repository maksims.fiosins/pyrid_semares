#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
@author: Moritz F P Becker
@modified: Maksims Fiosins
"""

import numpy as np
import pyrid as prd

import argparse
import yaml

import hiyapyco


def fill_defaults(args, data, keys):
   for key in keys:
      if key in data:
          args[key] = data[key]

parser = argparse.ArgumentParser(
                    prog='PyRID with yaml config',
                    description='PyRID with yaml config')

parser.add_argument('--config',
                    help='config file (yaml)')
parser.add_argument('--config_2',
                    help='additional config file (yaml)', nargs='?')
parser.add_argument('--output_folder',
                    help='output folder')
parser.add_argument('--compartments_file',
                    help='compartments file')
parser.add_argument('--compartments_scale',
                    help='scale of the compartments file')
parser.add_argument('--simulation_name',
                    help='name of the simulation')


args = parser.parse_args()

#%%

#-----------------------------------------------------
# Set Parameters
#-----------------------------------------------------

file_path = args.output_folder
fig_path = args.output_folder
file_name = args.simulation_name

if args.config_2 is None: 
    with open(args.config, "r") as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
else:
   config = hiyapyco.load(args.config, args.config_2, method=hiyapyco.METHOD_MERGE, interpolate=True, failonmissingfiles=True)

print(prd.Simulation.__init__.__code__)

print("Creating Simulation")
config["general"]["file_name"] = file_name
config["general"]["file_path"] = file_path
config["general"]["fig_path"] = fig_path
Simulation = prd.Simulation(**config["general"])
print("Simulation created")
#%%

#sys.exit()

#-----------------------------------------------------
# Define Particle Types
#-----------------------------------------------------

print("Creating particles")
for particle in config["particles"]:
    Simulation.register_particle_type(particle, config["particles"][particle]["radius"]) # (Name, Radius)
print("Particles created")
#%%


#-----------------------------------------------------
# Define Molecule Structure
#-----------------------------------------------------

print("Creating molecules")
for molecule in config["molecules"]:
    if config["molecules"][molecule]["positions"]["type"] == "coordinates":
        A_pos = config["molecules"][molecule]["positions"]["coordinates"] 
    elif config["molecules"][molecule]["positions"]["type"] == "evenly_on_sphere":
        A_pos = prd.distribute_surf.evenly_on_sphere(config["molecules"][molecule]["positions"]["sphere"]["num_points"], config["molecules"][molecule]["positions"]["sphere"]["radius"])
    else:
        raise ValueError(f"unknown molecule position type {config['molecules'][molecule]['positions']['type']}. Allowed values are coordinates, evenly_on_sphere")
    A_types = config["molecules"][molecule]["particles"]
    dargs = {"molecule_name": molecule, "particle_pos": A_pos, "particle_types": A_types}
    fill_defaults(dargs, config["molecules"][molecule], ["collision_type", "h_membrane"])
    Simulation.register_molecule_type(**dargs)
    D_tt, D_rr = prd.diffusion_tensor(Simulation, molecule)
    Simulation.set_diffusion_tensor(molecule, D_tt, D_rr)
print("Molecules created")

print("Creating matrix plots")
mobility_matrix_plots = [plot for plot in config["plots"] if plot["type"] == "mobility_matrix"]
for plot in mobility_matrix_plots:
    prd.plot.plot_mobility_matrix(plot["molecule"], Simulation,  save_fig = True, show = False)
print("Matrix plots created")

#%%

#-----------------------------------------------------
# Add Global Pair Interactions
#-----------------------------------------------------

print("Creating interactions")
for interaction in config["interactions"]:
    for interaction_details in config["interactions"][interaction]:
         Simulation.add_interaction(interaction, interaction_details["particle_1"], interaction_details["particle_1"], interaction_details["parameters"], bond = interaction_details["bond"])
print("Interactions created")

#%%

#-----------------------------------------------------
# Add Pair Binding Reactions
#-----------------------------------------------------

if "bp_reactions" in config:
    print("Creating bp reactions")
    for bp_reaction in config["bp_reactions"]:
        for bp_reaction_details in config["bp_reactions"][bp_reaction]:
                dargs = {"reaction_type": bp_reaction, "educt_types": bp_reaction_details["educt"], "product_types": bp_reaction_details["product"], "rate": bp_reaction_details["rate"], "radius": bp_reaction_details["radius"]}
                fill_defaults(dargs, bp_reaction_details, ["interaction_type", "interaction_parameters"])
                Simulation.add_bp_reaction(**dargs)
    print("bp reactions created")
else:
    print("bp reactions skipped")

#%%

if "um_reactions" in config:
    print("Creating um reactions")
    for um_reaction in config["um_reactions"]:
        for um_reaction_details in config["um_reactions"][um_reaction]:
                dargs = {"reaction_type": um_reaction, "educt_type": um_reaction_details["educt"], "product_types": um_reaction_details["product"], "rate": um_reaction_details["rate"]}
                fill_defaults(dargs, um_reaction_details, ["product_loc", "product_direction","radius"])
                Simulation.add_um_reaction(**dargs)
    print("um reactions created")
else:
    print("um reactions skipped")

#%%

if "up_reactions" in config:
    print("Creating up reactions")
    for up_reaction in config["up_reactions"]:
        for up_reaction_details in config["up_reactions"][up_reaction]:
                dargs = {"reaction_type": up_reaction, "educt_type": up_reaction_details["educt"], "product_types": up_reaction_details["product"], "rate": up_reaction_details["rate"]}
                fill_defaults(dargs, up_reaction_details, ["product_loc", "product_direction","radius"])
                Simulation.add_up_reaction(**dargs)
    print("up reactions created")
else:
    print("up reactions skipped")
#%%

if "bm_reactions" in config:
    print("Creating bm reactions")
    for bm_reaction in config["bm_reactions"]:
        for bm_reaction_details in config["bm_reactions"][bm_reaction]:
                print(bm_reaction_details)
                dargs = {"reaction_type": bm_reaction, "educt_types": bm_reaction_details["educt"], "product_types": bm_reaction_details["product"], "particle_pairs": bm_reaction_details["particle_pairs"], "pair_rates": bm_reaction_details["pair_rates"], "pair_radii": bm_reaction_details["pair_radii"]}
                fill_defaults(dargs, bm_reaction_details, ["placement_factor"])
                Simulation.add_bm_reaction(**dargs)
    print("bm reactions created")
else:
    print("bm reactions skipped")

#%%

print("Creating potential plots")
potential_plots = [plot for plot in config["plots"] if plot["type"] == "potential"]
#for plot in potential_plots:
#    prd.plot.plot_potential(Simulation, plot["potential"], yU_limits = plot["yU_limits"], yF_limits = plot["yF_limits"], r_limits = plot["r_limits"], save_fig = True, show = False)
print("Potential plots created")

#-----------------------------------------------------
# Import Compartments
#-----------------------------------------------------

print("Loading compartments")
vertices, triangles, Compartments = prd.load_compartments(args.compartments_file)
print("Compartments loaded")
print("Adding compartments to simulation")
Simulation.set_compartments(Compartments, triangles, vertices, mesh_scale = float(args.compartments_scale))
print("Compartments loaded")
print("Creating compartment plots")
compartment_plots = [plot for plot in config["plots"] if plot["type"] == "compartments"]
for plot in compartment_plots:
    prd.plot.plot_compartments(Simulation, save_fig = True, show = False)
print("Compartment plots added")

#%%

#-----------------------------------------------------
# Release events
#-----------------------------------------------------

if "release_sites" in config:
    print("Creating release sites")
    for release_site in config["release_sites"]:
        for rs_details in config["release_sites"][release_site]:
                dargs = {"Location": release_site, "timepoint": rs_details["timepoint"], "compartment_number": rs_details["compartment"]}
                fill_defaults(dargs, rs_details, ["Number", "Types","origin", "jitter", "Molecules_id", "Positions", "Quaternion"])
                if "triangle" in rs_details:
                    compartment_id = rs_details["triangle"]["compartment"]
                    triangle_id = rs_details["triangle"]["triangle_id"]
                    dargs["triangle_id"] = Simulation.System.Compartments[compartment_id].triangle_ids[triangle_id]
                Simulation.add_release_site(**dargs)
    print("release sites created")
else:
    print("release sites skipped")

#-----------------------------------------------------
# Fixed concentration at boundary
#-----------------------------------------------------

if "concentrations" in config:
    print("Creating concentrations")
    for concentration in config["concentrations"]:
        if concentration["type"] == "fixed_at_boundary":
            Simulation.fixed_concentration_at_boundary(concentration["molecule"], concentration["concentration"], concentration["compartment"], concentration["location"])
    print("Concentrations added")
else:
    print("Concentrations skipped")
#%%          

#-----------------------------------------------------
# Distribute Molecules
#-----------------------------------------------------

print("Creating distributions")
for distribution in config["molecule_distributions"]:
    dargs = {"Method": distribution["method"], 
            "Location": distribution["location"], 
            "Compartment_Number": distribution.get("compartment_distribute", distribution["compartment"]),
            "Types": list(distribution["molecules"].keys()), 
            "Number": [distribution["molecules"][molecule] for molecule in distribution["molecules"].keys()], 
            }
    fill_defaults(dargs, distribution, ["clustering_factor", "max_trials", "jitter", "triangle_id", "multiplier", "facegroup"])
    print("Distribution parameters")
    print(dargs)
    res = Simulation.distribute(**dargs)	# Needs to be changed in PyRID: always return a tuple of 4 elements
    face_ids = None
    pos, mol_type_idx, quaternion = res[:3]
    if len(res) == 4:
        face_ids = res[3]
    print("*****Pos*****")
    print(pos)
    print("****Mol_type_ids*****")
    print(mol_type_idx)
    print("****quaternion******")
    print(quaternion)
    print("****face_ids*****")
    print(face_ids)
    Simulation.add_molecules(distribution["location"],distribution["compartment"], pos, quaternion, mol_type_idx, face_ids)
print("Distributions created")

#%%

print("Creating scene plots")
scene_plots = [plot for plot in config["plots"] if plot["type"] == "scene"]
for plot in scene_plots:
    prd.plot.plot_scene(Simulation, save_fig = True, show = False)
print("Scene plots created")
#%%

#-----------------------------------------------------
# Add Observables
#-----------------------------------------------------

if "observe_rdf" in config:
    print("Adding observe rdf")
    Simulation.observe_rdf(rdf_pairs = config["observe_rdf"]["rdf_pairs"], rdf_bins = config["observe_rdf"]["rdf_bins"], rdf_cutoff = config["observe_rdf"]["rdf_cutoff"], stride = config["observe_rdf"]["stride"])
    print("observe rdf added")
else:
    print("observe rdf skipped")    

if "observations" in config:
    print("Adding observables")
    for observation in config["observations"]:
        dargs = {"Measure": observation["measure"]}
        fill_defaults(dargs, observation, ["molecules","reactions","obs_stride","stepwise","binned"])
        Simulation.observe(**dargs)
    print("Observables added")
else:
    print("Observables skipped")
#%%

#-----------------------------------------------------
# Start the Simulation
#-----------------------------------------------------

print("Running simulation")
dargs = {}
fill_defaults(dargs, config["run"], ["progress_stride", "keep_time", "out_linebreak", "progress_bar_properties", "start_reactions"])

Timer = Simulation.run(**dargs)
print("Simulation completed")

Simulation.print_timer()


#%%

print("Creating concentration plots")
concentration_plots = [plot for plot in config["plots"] if plot["type"] == "concentration"]
for plot in concentration_plots:
    prd.plot.plot_concentration_profile(Simulation, axis = plot["axis"], save_fig = True, show = False)
print("Concentration plots created")

if "evaluation" in config:
    print("Creating evaluation")
    Evaluation = prd.Evaluation(path = args.output_folder)
    Evaluation.load_file(file_name, path = args.output_folder)
    print("Creating reaction plots")
    reaction_plots = [plot for plot in config["plots"] if plot["type"] == "reaction"]
    for plot in reaction_plots:
        Evaluation.plot_reactions_graph(Simulation, graph_type = plot, save_fig = True, show = False)
    print("Reaction plots created")
    if "read_observable" in config["evaluation"]:
        for evaluation_details in config["evaluation"]["read_observable"]:
            dargs = {"measure": evaluation_details["measure"]}
            fill_defaults(dargs, evaluation_details, ["sampling", "molecules", "educts", "Reaction_Type", "steps"])
            Evaluation.read_observable(**dargs)

    if "plot_observable" in config["evaluation"]:
        for evaluation_details in config["evaluation"]["plot_observable"]:
            dargs = {"measure": evaluation_details["measure"], "save_fig": True, "show": False}
            fill_defaults(dargs, evaluation_details, ["molecules", "Reaction_Type", "educt", "bond_pairs", "particle_educt", "step"])
            Evaluation.plot_observable(**dargs)
    print("Evaluation created")
else:
    print("Evaluation skipped")

print("Simulation completed")
